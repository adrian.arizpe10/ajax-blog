(function () {
"use strict";


    $(document).ready(function(){
        $.ajax({
            url: 'data/my-ajax-posts.json',
            type: 'GET',
            dataType: 'JSON',
            success: function(response){
                var len = response.length;
                for(var i=0; i<len; i++){
                    var title = response[i].title;
                    var content = response[i].content;
                    var category = response[i].category;
                    var date = response[i].date;


                    var all = "<h3 align='center'>" + title + "</h3>" +
                        "<br>" +
                        "<p>" + content + "</p>" +
                        "<strong> Category: </strong>" +
                        "<div align='bottom-left'>" + category + "</div>" +
                        "<br>" +
                        "<strong> Date: </strong>" +
                        "<i align='right'>" + date + "</i>";

                    $("#my-posts").append(all);
                }

            }
        });
    });

})()